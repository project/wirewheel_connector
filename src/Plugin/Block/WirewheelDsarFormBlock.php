<?php

namespace Drupal\wirewheel_connector\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\wirewheel_connector\FetchForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a wirewheel form block.
 *
 * @Block(
 *   id = "wirewheel_connector_form",
 *   admin_label = @Translation("WireWheel DSAR Form"),
 *   category = @Translation("Custom")
 * )
 */
class WirewheelDsarFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Fetch Form service.
   *
   * @var \Drupal\wirewheel_connector\FetchForm
   */
  protected $fetchForm;

  /**
   * Form builder will be used via Dependency Injection.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Current Route Match will be used via Dependency Injection.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new ExampleBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\wirewheel_connector\FetchForm $fetch_form
   *   The fetch form service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The fetch form service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FetchForm $fetch_form, FormBuilderInterface $form_builder, ConfigFactory $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fetchForm = $fetch_form;
    $this->formBuilder = $form_builder;
    $this->config = $configFactory->get('wirewheel_connector.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wirewheel_connector.fetch_form'),
      $container->get('form_builder'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Form ID.
    $form['form_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form ID'),
      '#default_value' => $this->configuration['form_id'],
    ];
    $form['show_title'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Title'),
      '#default_value' => $this->configuration['show_title'],
      '#description' => $this->t('Includes the title of your Privacy Studio Page when rendering your form.'),
    ];
    $form['show_description'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Description'),
      '#default_value' => $this->configuration['show_description'],
      '#description' => $this->t('Includes the description of your Privacy Studio Page when rendering your form.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['form_id'] = $form_state->getValue('form_id');
    $this->configuration['show_title'] = $form_state->getValue('show_title');
    $this->configuration['show_description'] = $form_state->getValue('show_description');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Base URL.
    $site_base_url = $this->config->get('instance_id');

    // API Url.
    $api_url = $this->generateApiUrl();

    // Recaptcha API Information.
    $recaptcha_api_information = $this->config->get('recaptcha_api_information');

    // Recaptcha site Key.
    $recaptcha_api = $site_base_url . '/' . $recaptcha_api_information;
    $recaptcha_site_key = $this->fetchForm->getRecaptchaSiteKey($recaptcha_api);

    // Wirewheel Endpoint.
    $data_endpoint = $this->config->get('data_endpoint');

    // Form ID and Site Base URL.
    $api_information = [
      'form_id' => $this->configuration['form_id'],
      'show_title' => $this->configuration['show_title'],
      'show_description' => $this->configuration['show_description'],
      'site_base_url' => $site_base_url,
      'recaptcha_site_key' => $recaptcha_site_key,
      'data_endpoint' => $data_endpoint,
    ];

    // Different Request Types.
    $request_types = $this->fetchForm->getRequestTypes($api_url);

    // Form Fields.
    $form_fields = $this->fetchForm->getFormFields($api_url);

    // Form header.
    $form_header = $this->fetchForm->getFormHeaders($api_url);

    $form = $this->formBuilder->getForm('Drupal\wirewheel_connector\Form\WirewheelForm', $request_types, $form_fields, $form_header, $api_information);
    $form['#attached']['library'][] = 'wirewheel_connector/wirewheel_connector';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Generate the API URL for path.
   *
   * @return string
   *   API URL.
   */
  private function generateApiUrl() {

    // Form ID.
    $form_id = $this->configuration['form_id'];

    // Global Configuration.
    $site_base_url = $this->config->get('instance_id');
    $site_data_path = $this->config->get('data_path');

    $api_url = $site_base_url . '/' . $site_data_path . '/' . $form_id;
    return $api_url;
  }

}
