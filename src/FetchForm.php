<?php

namespace Drupal\wirewheel_connector;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * FetchForm service to fetch form details.
 */
class FetchForm {

  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a FetchForm object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $http_client;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Method description.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response object.
   */
  public function getResponse($api_url) {
    $function_id = \base64_encode(__FUNCTION__ . $api_url);
    $response = &drupal_static($function_id);
    if (isset($response)) {
      return $response;
    }
    try {
      $request = $this->httpClient->request('GET', $api_url);
      if ($request->getStatusCode() === 200) {
        $response = json_decode($request->getBody());
        return $response;
      }
    }
    catch (HttpException $exception) {
      // @todo Handle exceptions here.
    }
  }

  /**
   * Get all type of requests.
   *
   * @param string $api_url
   *   API URL.
   */
  public function getRequestTypes($api_url) {
    $response = $this->getResponse($api_url);
    if (isset($response->srrConfig) && isset($response->srrConfig->requests)) {
      $requests = $response->srrConfig->requests;

      // Get the request information.
      $request_information = $this->getTitleDescriptionRequestTypes();

      $request_types = [];
      foreach ($requests as $request) {
        if (!array_key_exists($request->requestType, $request_information)) {
          continue;
        }
        $request_types[$request->requestType] = $request_information[$request->requestType];
      }
    }
    return $request_types;
  }

  /**
   * Get the Title and Description for all the Request Types.
   */
  public function getTitleDescriptionRequestTypes() {
    $request_information = [
      'access' => [
        'title' => $this->t('Access'),
        'description' => $this->t('Request access to your personal information.'),
      ],
      'deletion' => [
        'title' => $this->t('Deletion'),
        'description' => $this->t('Request your user data be deleted from our systems.'),
      ],
      'optOut' => [
        'title' => $this->t('Opt-out of Data Processing'),
        'description' => $this->t('Withdraw consent for the processing of your personal data.'),
      ],
      'optOut-data-processing' => [
        'title' => $this->t('Opt-out of Data Processing'),
        'description' => $this->t('Stop processing my data.'),
      ],
      'do-not-sell' => [
        'title' => $this->t('Do not sell my personal information'),
        'description' => $this->t('Withdraw consent for the sale of your personal information.'),
      ],
      'category-access' => [
        'title' => $this->t('Category Access'),
        'description' => $this->t('Request information on which types of data have been collected about you.'),
      ],
      'correction' => [
        'title' => $this->t('Correction'),
        'description' => $this->t('Request we rectify inaccurate or incomplete personal data about you.'),
      ],
      'portability' => [
        'title' => $this->t('Portability'),
        'description' => $this->t('Request we provide your personal data in a machine-readable format.'),
      ],
    ];

    return $request_information;
  }

  /**
   * Get form fields for a given requestType.
   *
   * @param string $api_url
   *   URL for the API.
   *
   * @return array
   *   Response array.
   */
  public function getFormFields($api_url) {
    $response = $this->getResponse($api_url);
    if (isset($response->srrConfig) && isset($response->srrConfig->requests)) {
      $requests = $response->srrConfig->requests;
      foreach ($requests as $request) {
        foreach ($request->fields as $dataset) {
          $data = (array) $dataset;
          $apiIdentifier = $data['apiIdentifier'];

          if (!empty($elements[$apiIdentifier])) {
            $elements[$apiIdentifier]['ids'][$request->requestType] = $data['_id'];
          }
          else {
            $data['ids'] = [
              $request->requestType => $data['_id'],
            ];
            $elements[$apiIdentifier] = $data;
          }
        }
      }
    }
    return $elements;
  }

  /**
   * Get the Form Headers.
   *
   * @param string $api_url
   *   URL for the API.
   *
   * @return array
   *   Response array.
   */
  public function getFormHeaders($api_url) {
    $response = $this->getResponse($api_url);

    // Header Text.
    if (isset($response->srrConfig) && isset($response->srrConfig->header)) {
      $header_information['header'] = $response->srrConfig->header;
    }

    // Description for the form.
    if (isset($response->srrConfig) && isset($response->srrConfig->description)) {
      $header_information['description'] = $response->srrConfig->description;
    }

    // Success Message from the API.
    if (isset($response->srrConfig) && isset($response->srrConfig->successMessage)) {
      $header_information['successMessage'] = $response->srrConfig->successMessage;
    }

    // Recaptcha Enabled or not.
    $header_information['enableRecaptcha'] = $response->srrConfig->enableRecaptcha;

    return $header_information;
  }

  /**
   * Get the Recaptcha SiteKey.
   *
   * @param string $api_url
   *   URL for the API.
   *
   * @return array
   *   Response array.
   */
  public function getRecaptchaSiteKey($api_url) {
    $response = $this->getResponse($api_url);

    // Recaptcha Sitekey.
    $recaptcha_sitekey = $response->recaptchaSiteKey;

    return $recaptcha_sitekey;
  }

  /**
   * Send Post API Response.
   *
   * @param string $endpoint
   *   API Endpoint to send request.
   * @param array $data
   *   Data to sent to API.
   *
   * @return object
   *   Response object.
   */
  public function sendApiResponse($endpoint, array $data) {
    try {
      $response = $this->httpClient->request('POST', $endpoint, [
        'body' => \json_encode($data),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
      ]);

      if (in_array($response->getStatusCode(), [200, 201, 202])) {
        return json_decode($response->getBody());
      }

    }
    catch (RequestException $e) {
      // Handle 4xx http responses.
      if ($response = $e->getResponse()) {
        if ($response->getStatusCode() == 400) {
          $this->loggerFactory->get('wirewheel_connector')->notice('HTTP request to @endpoint failed with error: @error.', [
            '@endpoint' => $endpoint,
            '@error' => $response->getStatusCode() . ' ' . $response->getReasonPhrase(),
          ]);
        }
      }
    }
  }

}
