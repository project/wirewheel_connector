<?php

namespace Drupal\wirewheel_connector\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\wirewheel_connector\FetchForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Wirewheel form.
 */
class WirewheelForm extends FormBase {

  /**
   * The Fetch Form service.
   *
   * @var \Drupal\wirewheel_connector\FetchForm
   */
  protected $fetchForm;

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function __construct(FetchForm $fetch_form, ConfigFactory $configFactory, RendererInterface $renderer, Request $request) {
    $this->fetchForm = $fetch_form;
    $this->config = $configFactory->get('wirewheel_connector.settings');
    $this->renderer = $renderer;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('wirewheel_connector.fetch_form'),
      $container->get('config.factory'),
      $container->get('renderer'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wirewheel_connector';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $request_types = [], $form_fields = [], $form_header = [], $api_information = []) {

    // Attach the Default CSS library.
    $attach_default_styling = $this->config->get('default_styling');
    if ($attach_default_styling) {
      $form['#attached']['library'][] = 'wirewheel_connector/wirewheel_form';
    }

    $id_identifier_map = [];

    foreach ($form_fields as $key => $field) {
      $id_identifier_map[$field['apiIdentifier']]['ids'] = $field['ids'];
      $id_identifier_map[$field['apiIdentifier']]['type'] = $field['type'];
      $id_identifier_map[$field['apiIdentifier']]['label'] = $field['label'];
      if (!empty($field['ruleOptions'])) {
        $id_identifier_map[$field['apiIdentifier']]['rules'] = $field['rules'];
        $id_identifier_map[$field['apiIdentifier']]['ruleOptions'] = $field['ruleOptions'];
      }
    }

    // Fetch the Header information.
    $header = '';
    if ($api_information['show_title']) {
      $header .= '<h3>' . $form_header['header'] . '</h3>';
    }

    if ($api_information['show_description']) {
      $header .= '<div class="header-description">' . $form_header['description'] . '</div>';
    }

    $form['header'] = [
      '#markup' => $header,
      '#prefix' => '<div class="header-wrapper">',
      '#suffix' => '</div>',
    ];

    $storage = [
      'form_field_elements' => $id_identifier_map,
      'api_information' => $api_information,
      'request_types' => array_keys($request_types),
      'successMessage' => $form_header['successMessage'],
      'enableRecaptcha' => $form_header['enableRecaptcha'],
    ];

    $form_state->setStorage($storage);

    // Add a wrapper to the form.
    $form['#prefix'] = '<div class="wirewheel-form-wrapper container">';
    $form['#suffix'] = '</div>';

    $count = 0;
    // Get the Title for the Categories.
    foreach ($request_types as $key => $request_type) {
      $options[$key] = $request_type['title']->render();
      $form[$key] = [
        '#type' => 'checkbox',
        '#title' => $request_type['title']->render(),
        '#description' => $request_type['description']->render(),
        '#suffix' => '</div>',
      ];

      if (($count % 2) === 0) {
        $form[$key]['#prefix'] = '<div class="request-types wirewheel-even">';
      }
      else {
        $form[$key]['#prefix'] = '<div class="request-types wirewheel-odd">';
      }
      $count++;
    }

    // Enable submit only if one of the request type is selected.
    $request_type_state = [];
    foreach (array_keys($request_types) as $type) {
      $state = [
        ':input[name="' . $type . '"]' => ['checked' => TRUE],
      ];
      \array_push($request_type_state, $state);
    }

    if (!empty($form_fields)) {

      $form['prefix_form_element_wrapper'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#prefix' => '<div class="wirewheel-col-12">',
        '#states' => [
          'visible' => $request_type_state,
        ],
      ];

      foreach ($form_fields as $field) {
        $api_identfier = $field['apiIdentifier'];
        $states = [];
        $field_applicable_to = \array_keys($field['ids']);

        if (count($field_applicable_to) > 1) {
          foreach ($field_applicable_to as $type) {
            $state = [
              ':input[name="' . $type . '"]' => ['checked' => TRUE],
            ];
            \array_push($states, $state);
          }
        }
        else {
          foreach ($request_types as $key => $request_type) {
            unset($form[$key]);
          }
        }

        switch ($field['type']) {
          case 'text':
            $type = 'textfield';
            break;

          case 'radio':
            $type = 'radios';
            break;

          case 'checkbox':
            $type = 'checkboxes';
            break;

          default:
            $type = $field['type'];
        }

        $form['prefix_form_element_wrapper'][$api_identfier] = [
          '#type' => $type,
          '#title' => $this->t('@title', ['@title' => $field['label']]),
          '#required' => (bool) $field['required'],
          '#states' => [
            'visible' => $states,
            'required' => $states,
          ],
          '#prefix' => '<div class="wirewheel-col-6 form-element-wrapper">',
          '#suffix' => '</div>',
          '#attributes' => [
            'placeholder' => $this->t('@title', ['@title' => $field['label']]),
          ],
        ];

        // WireWheel field rules integration.
        if (!empty($field['rules'])) {
          $rule_options = $field['ruleOptions'];
          if ($field['type'] === 'text') {
            if (in_array('number-only', $field['rules'])) {
              $form['prefix_form_element_wrapper'][$api_identfier]['#pattern'] = "\d*";
              $form['prefix_form_element_wrapper'][$api_identfier]['#description'] = $this->t('Number values only.');
            }
            if (in_array('letters-only', $field['rules'])) {
              $form['prefix_form_element_wrapper'][$api_identfier]['#pattern'] = "^[a-zA-Z]+$";
              $form['prefix_form_element_wrapper'][$api_identfier]['#description'] = $this->t('Alphabetical characters only.');
            }
            if (!empty($rule_options->limit)) {
              $form['prefix_form_element_wrapper'][$api_identfier]['#maxlength'] = $rule_options->limit;
            }
          }

          if ($field['type'] === 'date') {
            if (!empty($rule_options->min)) {
              $form['prefix_form_element_wrapper'][$api_identfier]['#min'] = $rule_options->min;
            }
            if (!empty($rule_options->max)) {
              $form['prefix_form_element_wrapper'][$api_identfier]['#max'] = $rule_options->max;
            }
          }
        }
        if ($field['type'] === 'date') {
          $form['prefix_form_element_wrapper'][$api_identfier]['#attributes']['type'] = 'date';
        }

        if ($type == 'select' || $type == 'radios' || $type == 'checkboxes') {

          $options = [];
          // Create the Options array.
          foreach ($field['items'] as $item) {
            $options[$item] = $item;
          }

          $form['prefix_form_element_wrapper'][$api_identfier]['#options'] = $options;
        }
      }
    }

    // Attach Recaptcha if required.
    if ($form_header['enableRecaptcha']) {

      // Attach Recaptcha library.
      $form['#attached']['library'][] = 'wirewheel_connector/wirewheel_form_recaptcha';

      // Attach the Recaptcha to the form.
      $form['prefix_form_element_wrapper']['recaptcha'] = [
        '#markup' => '<div id="recaptcha-setup" class="recaptcha"></div>',
      ];

      // Pass Recaptcha Site Key to Drupal settings.
      $form['#attached']['drupalSettings']['wirewheel_connector']['recaptcha_site_key'] = $api_information['recaptcha_site_key'];
    }

    $form['prefix_form_element_wrapper']['actions'] = [
      '#type' => 'actions',
      '#prefix' => '<div class="wirewheel-col-12 wirewheel-submit">',
      '#suffix' => '</div></div>',
    ];

    $form['prefix_form_element_wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#states' => [
        'visible' => $request_type_state,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $storage = $form_state->getStorage();
    $values = $form_state->getValues();
    // Validate text field character length.
    if (!empty($storage['form_field_elements']['text']['ruleOptions']) && !empty($values['text']) && strlen($values['text']) < $storage['form_field_elements']['text']['ruleOptions']->min) {
      $form_state->setError($form['prefix_form_element_wrapper']['text'], $this->t('Minimum character count not met, @entered_num number of characters found, but expected at least @expected_num.', [
        '@entered_num' => strlen($values['text']),
        '@expected_num' => $storage['form_field_elements']['text']['ruleOptions']->min,
      ]
      ));
    }

    // Validate number field character length.
    if (!empty($storage['form_field_elements']['number']['ruleOptions']) && !empty($values['number']) && strlen($values['number']) < $storage['form_field_elements']['number']['ruleOptions']->min) {
      $form_state->setError($form['prefix_form_element_wrapper']['text'], $this->t('Minimum character count not met, @entered_num number of characters found, but expected at least @expected_num.', [
        '@entered_num' => strlen($values['text']),
        '@expected_num' => $storage['form_field_elements']['number']['ruleOptions']->min,
      ]
      ));
    }
    if (!empty($storage['form_field_elements']['number']['ruleOptions']) && !empty($values['number']) && strlen($values['number']) > $storage['form_field_elements']['number']['ruleOptions']->limit) {
      $form_state->setError($form['prefix_form_element_wrapper']['text'], $this->t('Maximum character count not met, @entered_num number of characters found, but expected only @expected_num.', [
        '@entered_num' => strlen($values['text']),
        '@expected_num' => $storage['form_field_elements']['number']['ruleOptions']->limit,
      ]
      ));
    }
    $recaptcha_response_token = $this->request->get('g-recaptcha-response');
    if ($storage['enableRecaptcha'] && empty($recaptcha_response_token)) {
      $form_state->setError($form['prefix_form_element_wrapper'], $this->t('Recaptcha is required.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, $return = FALSE) {

    $storage = $form_state->getStorage();

    // Third Party form ID.
    $third_party_form_id = $storage['api_information']['form_id'];

    // API URL.
    $site_base_url = $storage['api_information']['site_base_url'];

    // Wirewheel Endpoint.
    $endpoint = $storage['api_information']['data_endpoint'];
    $api_url = $site_base_url . '/' . $endpoint;

    // Request Types.
    $values = $form_state->getValues();
    $all_request_types = $storage['request_types'];

    // Success Message.
    $success_message = $storage['successMessage'];

    // Get selected request types.
    $selected_request_type = [];

    if (count($all_request_types) > 1) {
      foreach ($all_request_types as $request_type) {
        if (!$values[$request_type]) {
          continue;
        }
        \array_push($selected_request_type, $request_type);
      }
    }
    else {
      $selected_request_type = $all_request_types;
    }

    foreach ($storage['form_field_elements'] as $api_indentifier => $dataset) {
      // Data value.
      $api_identifier_value = $form_state->getValue($api_indentifier);

      if ($dataset['type'] == 'checkbox') {
        $values = [];
        foreach ($api_identifier_value as $value) {
          if ($value) {
            array_push($values, $value);
          }
          $api_identifier_value = $values;
        }
      }

      // Email Address.
      if ($api_indentifier == 'email') {
        $different_emails[] = $form_state->getValue($api_indentifier);
      }

      // List of different IDs.
      $data = array_keys($dataset['ids']);

      // Information for different request Types.
      foreach ($selected_request_type as $request_type) {
        if (in_array($request_type, $data)) {
          $submitted_dataset[] = [
            'value' => $api_identifier_value,
            'label' => $dataset['label'],
            '_id' => $dataset['ids'][$request_type],
          ];
        }
      }
    }

    // Get the list of all the emails.
    $mails = '';
    if (isset($different_emails)) {
      foreach ($different_emails as $mail) {
        if (end($different_emails) !== $mail) {
          $mails .= $mail . ',';
        }
        else {
          $mails .= $mail;
        }
      }
    }

    // Recaptcha Response Token.
    $recaptcha_response_token = '';
    if ($storage['enableRecaptcha']) {
      // Recaptcha Response Token.
      $recaptcha_response_token = $this->request->get('g-recaptcha-response');
    }

    // Result array.
    $result = [
      'data' => [
        'providedFields' => $submitted_dataset,
        'action' => '',
        'primaryEmail' => $mails,
        'providedRequests' => $selected_request_type,
        'recaptchaToken' => $recaptcha_response_token,
        'locale' => 'en',
      ],
      'meta' => [
        'id' => $third_party_form_id,
        'type' => 'dsar',
      ],
    ];

    $response = $this->fetchForm->sendApiResponse($api_url, $result);

    if ($return) {
      return $response;
    }
    // Message to the end user.
    $markup = $this->t('Here is your request ID: @requestID', ['@requestID' => $response->rootId]);

    $markup .= '<br>' . $success_message;

    $build = [
      '#type' => 'container',
      '#markup' => $markup,
    ];
    $message = $this->renderer->renderPlain($build);

    $this->messenger()->addStatus($message);
  }

}
