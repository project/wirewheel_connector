<?php

namespace Drupal\wirewheel_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a global configuration settings form.
 */
class WirewheelGlobalConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'wirewheel_connector.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wirewheel_connector_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    // Instance ID.
    $form['instance_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WireWheel Instance ID'),
      '#default_value' => $config->get('instance_id'),
      '#required' => TRUE,
      '#description' => $this->t('Please enter your WireWheel instance identifier. This will be the subdomain value in the URL. (Ex. https://example.wirewheel.io identifier would be “example”)'),
    ];

    // Data Path.
    $form['data_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WireWheel Data path'),
      '#default_value' => $config->get('data_path'),
      '#required' => TRUE,
      '#description' => $this->t('Please enter the relative path of the WireWheel data path identifier. The path should not start with a "/"'),
    ];

    // Recaptcha API Information.
    $form['recaptcha_api_information'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recaptcha Sitekey API link'),
      '#default_value' => $config->get('recaptcha_api_information'),
      '#required' => TRUE,
      '#description' => $this->t('Please enter the relative path of the WireWheel Recaptcha Sitekey. The path should not start with a "/"'),
    ];

    // Data Endpoint.
    $form['data_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WireWheel Data endpoint'),
      '#default_value' => $config->get('data_endpoint'),
      '#required' => TRUE,
      '#description' => $this->t('Please enter the relative path of the WireWheel endpoint. The path should not start with a "/"'),
    ];

    // Add Default Styling.
    $form['default_styling'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('default_styling'),
      '#title' => $this->t('Use Default Styling'),
      '#description' => $this->t('Uncheck this checkbox if you want to use the default module styling.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $field_values = $form_state->getValues();

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('instance_id', $field_values['instance_id'])
      ->set('data_path', $field_values['data_path'])
      ->set('default_styling', $field_values['default_styling'])
      ->set('recaptcha_api_information', $field_values['recaptcha_api_information'])
      ->set('data_endpoint', $field_values['data_endpoint'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
