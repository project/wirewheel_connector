/*
 * Recaptcha Token.
 */

var widgetId;
var onloadCallback = function () {
  widgetId = grecaptcha.render('recaptcha-setup', {
    'sitekey': drupalSettings.wirewheel_connector.recaptcha_site_key,
    'theme': 'light',
  });
};
